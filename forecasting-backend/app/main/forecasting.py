from clickhouse_driver import connect
import pandas as pd
from prophet import Prophet

conn = connect('clickhouse://analytics_ro:analytics_ro%40123@localhost:9000/default')
cursor = conn.cursor()
# cursor.execute('SELECT reporting_time, loaded_impressions, views, clicks, slot_id FROM fcc_ads_fccpreprod.l1_reporting_distributed')
cursor.execute('select toDate(reporting_time), sum(views), sum(loaded_impressions) from fcc_ads_noon.l1_reporting_distributed group by toDate(reporting_time)')
data = cursor.fetchall()
df = pd.DataFrame(data, columns=['date', 'views', 'impressions'])

df['date'] = pd.to_datetime(df['date'])

df_views = df[['date', 'views']]
df_impressions = df[['date', 'impressions']]
df_impressions.to_csv("impressions.csv")
df_views.to_csv("views.csv")
df_impressions.columns = ['ds', 'y']
print(df)
print(df_views)
print(df_impressions)

model = Prophet()
model.fit(df_impressions)
future = model.make_future_dataframe(periods=30)
forecast = model.predict(future)
print(df_impressions.dtypes)
print(forecast.dtypes)
print(forecast.columns)
forecast = pd.merge(forecast, df_impressions, on='ds', how='inner').fillna(0)
response = forecast[['ds', 'y', 'yhat', 'yhat_lower', 'yhat_upper']]
data = []
for index, row in response.iterrows():
    obj = {
        "date": row["ds"],
        "y": row["y"],
        "yhat": row["yhat"],
        "yhat_lower": row["yhat_lower"],
        "yhat_upper": row["yhat_upper"]
    }
    data.append(obj)
print(response)
