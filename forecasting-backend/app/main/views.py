from flask import render_template, Blueprint
from clickhouse_driver import connect
import pandas as pd
from prophet import Prophet
from flask import Response
from flask import jsonify
from flask_cors import CORS, cross_origin
from flask import request


main = Blueprint(
    'main',
    __name__,
    template_folder='templates/main',
    url_prefix='/api'
)


@main.route('/view/forecast', methods=['GET'])
@cross_origin()
def view_forecast():
    slot_id = request.args.get('slotId')
    days = request.args.get('days')
    query = 'select toDate(reporting_time), sum(views) from fcc_ads_noon.l1_reporting_distributed group by toDate(reporting_time)'
    if slot_id is not None or len(slot_id) > 0:
        query = "select toDate(reporting_time), sum(views) from hackday.l1_reporting_temp where slot_id='%s' group by toDate(reporting_time), slot_id" % slot_id
    conn = connect('clickhouse://analytics_ro:analytics_ro%40123@localhost:9000/default')
    cursor = conn.cursor()
    cursor.execute(query)
    cursor.execute(query)
    data = cursor.fetchall()
    df = pd.DataFrame(data, columns=['date', 'views'])
    if slot_id is None:
        df = df.iloc[20:]
        df = df.iloc[:-10]
    df['date'] = pd.to_datetime(df['date'])
    df_views = df[['date', 'views']]
    df_views.columns = ['ds', 'y']

    yfs_sale = pd.DataFrame({'holiday': 'playoff','ds': pd.to_datetime([
        '2021-11-21', '2021-11-22', '2021-11-23','2021-11-24', '2021-11-25', '2021-11-26','2021-11-27', '2021-11-28', '2021-11-29', '2022-10-22', 
        '2022-10-23','2022-10-24']),'lower_window': 0,'upper_window': 1})
    df_views['cap'] = 100000000
    df_views['floor'] = 100
    model = Prophet(
        growth='logistic', changepoint_prior_scale=0.5, 
        holidays=yfs_sale,
        yearly_seasonality=True, 
        weekly_seasonality=True
        )
    model.add_country_holidays(country_name='AE')
    model.fit(df_views)
    future = model.make_future_dataframe(periods=int(days))
    future['cap'] = 100000000
    future['floor'] = 100
    forecast = model.predict(future)
    print(df_views.dtypes)
    print(forecast.dtypes)
    print(forecast.columns)
    forecast = pd.merge(forecast, df_views, on='ds', how='outer').fillna(0)
    response = forecast[['ds', 'y', 'yhat', 'yhat_lower', 'yhat_upper']]
    data = []
    for index, row in response.iterrows():
        obj = {
            "date": row["ds"].strftime('%Y-%m-%d 00:00:00'),
            "y": row["y"],
            "yhat": row["yhat"],
            "yhat_lower": row["yhat_lower"],
            "yhat_upper": row["yhat_upper"]
        }
        data.append(obj)
    print(response)

    return jsonify(data)



@main.route('/impression/forecast', methods=['GET'])
@cross_origin()
def impression_forecast():
    conn = connect('clickhouse://analytics_ro:analytics_ro%40123@localhost:9000/default')
    cursor = conn.cursor()
    # cursor.execute('SELECT reporting_time, loaded_impressions, views, clicks, slot_id FROM fcc_ads_fccpreprod.l1_reporting_distributed')
    cursor.execute('select toDate(reporting_time), sum(loaded_impressions) from fcc_ads_noon.l1_reporting_distributed group by toDate(reporting_time)')
    data = cursor.fetchall()
    df = pd.DataFrame(data, columns=['date', 'impressions'])
    df = df.iloc[20:]
    df = df.iloc[:-10]
    df['date'] = pd.to_datetime(df['date'])
    df_impressions = df[['date', 'impressions']]
    df_impressions.columns = ['ds', 'y']

    model = Prophet()
    model.fit(df_impressions)
    future = model.make_future_dataframe(periods=365)
    forecast = model.predict(future)

    forecast = pd.merge(forecast, df_impressions, on='ds', how='outer').fillna(0)
    response = forecast[['ds', 'y', 'yhat', 'yhat_lower', 'yhat_upper']]
    data = []
    for index, row in response.tail(30).iterrows():
        obj = {
            "date": row["ds"].strftime('%Y-%m-%d 00:00:00'),
            "y": row["y"],
            "yhat": row["yhat"],
            "yhat_lower": row["yhat_lower"],
            "yhat_upper": row["yhat_upper"]
        }
        data.append(obj)
    print(response)

    return jsonify(data)
