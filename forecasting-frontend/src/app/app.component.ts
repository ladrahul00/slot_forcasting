import { Component, ViewChild, ElementRef } from '@angular/core';
import { Chart, ChartConfiguration, ChartEvent, ChartType } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';
import {FormGroup, FormControl, FormBuilder} from '@angular/forms';
import { APIService } from './api.service';

const today = new Date();
const month = today.getMonth();
const year = today.getFullYear();

// import {default as Annotation} from 'chartjs-plugin-annotation';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-chartjs-example';
  slotForm = this.formBuilder.group({
    slotId: "HOME-PAGE-DAILY-UAE2",
    days: 365
  })
  @ViewChild('lineViewCanvas') lineViewCanvas: ElementRef | undefined;
  lineViewChart: any;
  @ViewChild('lineImpressionCanvas') lineImpressionCanvas: ElementRef | undefined;
  lineImpressionChart: any;
  campaignOne = new FormGroup({
    start: new FormControl(new Date(year, month, 13)),
    end: new FormControl(new Date(year, month, 16)),
  });
  campaignTwo = new FormGroup({
    start: new FormControl(new Date(year, month, 15)),
    end: new FormControl(new Date(year, month, 19)),
  });
  constructor(private apiService: APIService, private formBuilder: FormBuilder) {
    // Chart.register(Annotation)
  }

  public lineViewChartData: ChartConfiguration['data'] = {
    datasets: [
      {
        data: [ ],
        label: 'Predicted Views',
        fill: 'origin',
      },
      {
        data: [  ],
        label: 'Actual Views',
        fill: 'origin',
      }

    ],
    labels: [  ]
  };
  public lineViewChartOptions: ChartConfiguration['options'] = {
    elements: {
      line: {
        tension: 0.5
      }
    },
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      x: {
        type:"timeseries"
      },
      'y-axis-0':
        {
          position: 'left',
        },
      'y-axis-1': {
        position: 'right',
        grid: {
          color: 'rgba(255,0,0,0.3)',
        },
        ticks: {
          color: 'red'
        }
      }
    },


    plugins: {
      legend: { display: true }
    }
  };

  public lineChartType: ChartType = 'line';

  @ViewChild(BaseChartDirective) viewChart?: BaseChartDirective;
  @ViewChild(BaseChartDirective) impressionChart?: BaseChartDirective;

  private static generateNumber(i: number): number {
    return Math.floor((Math.random() * (i < 2 ? 100 : 1000)) + 1);
  }
  ngOnInit() {
    this.pullViewsData(this.slotForm.value.slotId!, this.slotForm.value.days!)
  }
  public pullViewsData(slotId?: string, days?:number) {
    this.apiService.getViewForecast(slotId, days).subscribe(res => {
      console.log(res);
      const labels = [];
      const data_y = [];
      const data_yhat = [];
      const data_yhat_lower = [];
      const data_yhat_upper = [];
      
      for (const vd of res) {
        labels.push(vd["date"]);
        data_y.push(vd["y"]);
        data_yhat.push(vd["yhat"]);
        data_yhat_lower.push(vd["yhat_lower"]);
        data_yhat_upper.push(vd["yhat_upper"]);
      }
      this.lineViewChartData.labels = labels;
      this.lineViewChartData.datasets[0].data = data_yhat;
      // this.lineViewChartData.datasets[1].data = data_yhat_upper;
      // this.lineViewChartData.datasets[2].data = data_yhat_lower;
      this.lineViewChartData.datasets[1].data = data_y;
      // console.log(this.lineChartData);
      this.viewChart?.update();
    });

    // this.apiService.getImpressionForecast().subscribe(res => {
    //   console.log(res);
    //   const labels = [];
    //   const data_yhat = [];
    //   const data_yhat_lower = [];
    //   const data_yhat_upper = [];
      
    //   for (const vd of res) {
    //     labels.push(vd["date"]);
    //     data_yhat.push(vd["yhat"]);
    //     data_yhat_lower.push(vd["yhat_lower"]);
    //     data_yhat_upper.push(vd["yhat_upper"]);
    //   }
    //   this.lineImpressionChartData.labels = labels;
    //   this.lineImpressionChartData.datasets[0].data = data_yhat;
    //   this.lineImpressionChartData.datasets[1].data = data_yhat_upper;
    //   this.lineImpressionChartData.datasets[2].data = data_yhat_lower;
    //   // console.log(this.lineChartData);
    //   this.impressionChart?.update();
    // });

  }

  onSubmit() {
    console.log(this.slotForm.value)
    this.pullViewsData(this.slotForm.value.slotId!, this.slotForm.value.days!)
  }


  // events
  public chartClicked({ event, active }: { event?: ChartEvent, active?: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event?: ChartEvent, active?: {}[] }): void {
    console.log(event, active);
  }
}

