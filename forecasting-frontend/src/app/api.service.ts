import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class APIService {
  constructor(private http: HttpClient) { }
  getViewForecast(slotId: string='', days:number=365) {
    return this.http.get<any>("http://localhost:5000/api/view/forecast?slotId=" + slotId + "&days="+days);
  }

  getImpressionForecast() {
    return this.http.get<any>("http://localhost:5000/api/impression/forecast");
  }
}